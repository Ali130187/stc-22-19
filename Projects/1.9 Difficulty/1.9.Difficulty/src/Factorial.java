public class Factorial {
    public static void main(String[] args) {
        System.out.println(getFactorialRecurse(6));
    }

    //5! = 1 * 2 * 3 * 4 * 5 = 120
    //3! = 1 * 2 * 3 = 6
    //6! = 1 * 2 * 3 * 4 * 5 * 6 = 720

    //Простая реализация высчитывания факториала
    //Последовательно умножаем переменную result на все числа до числа number
    public static int getFactorialFor(int number) {
        int result = 1;
        for (int i = 1; i <= number; i++) {
            result = result * i;
        }

        return result;
    }

    //Рекурсивно (внутри метода вызываем этот же метод) находим факториал числа
    //Суть в том, что мы рекурсивно вызываем метод, каждый раз кладем в него (число - 1)
    //Происходит это до тех пор, пока не дойдем до вызова данного метода, когда в него кладется 1
    //После этого, операторы return возвращают результаты работы методов в места, где они были вызваны
    //Т.е. вовзращается 1 и умножается на 2
    //После этого, результат произведения возвращается в место вызова и там, умножается на 3 и т.д. до числа,
    //чей факториал мы хотим найти
    public static int getFactorialRecurse(int number) {
        if (number == 1) {
            return number;
        }

        return number * getFactorialRecurse(number - 1);
    }

    //3! = 1 * 2 * 3 = 6
    //3! = 3 * 2 * 1 = 6
    /*
    public static int getFactorialRecurse(3) {
        if (3 == 1) { - ложь, мы не заходим в условие
            return number;
        }

        return 3 * 2;
    }

    public static int getFactorialRecurse(2) {
        if (2 == 1) { - ложь, мы не заходим в условие
            return number;
        }

        return 2 * 1;
    }

    public static int getFactorialRecurse(1) {
        if (1 == 1) { - истина, мы заходим в условие
            return 1;
        }

        return 2 * getFactorialRecurse(2 - 1 = 1);
    }
     */
}
